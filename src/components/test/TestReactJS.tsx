import React, { useCallback, useEffect, useState } from "react";
import { Pokemon, PokemonRes } from "../../type/pokemon.type";

const num = 50;
function TestReactJS() {
  const [dataRes, setDataRes] = useState<PokemonRes>();
  const [dataInitial, setDataInitial] = useState<Pokemon[]>([]);
  const [data, setData] = useState<Pokemon[]>([]);
  const [keyword, setKeyword] = useState<string>("");
  const [count, setCount] = useState<number>(1);
  const [offset, setOffset] = useState<number>(0);
  const [loading,setLoading] = useState<boolean>(true);

  const getPokemon = useCallback(async () => {
    setLoading(true);
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon?limit=50&&offset=${offset}`
    );
    const pkmRes: PokemonRes = await response.json();
    setDataRes(pkmRes);
    if (!!pkmRes?.results?.length) {
      setData(pkmRes?.results);
      setDataInitial(pkmRes?.results);
      const calcTotal = Math.ceil(pkmRes?.count / num);
      setCount(calcTotal);
    }
    setLoading(false);
  }, [offset]);
  useEffect(() => {
    let dataTemp = [...dataInitial];
    dataTemp = dataTemp?.filter((val) => val?.name?.includes(keyword));
    setData(dataTemp);
  }, [keyword, dataInitial]);

  useEffect(() => {
    getPokemon();
  }, [getPokemon]);

  useEffect(() => {
    setDataInitial([]);
    setData([]);
  }, [offset])
  

  const handleChangeKeyword = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setKeyword(value);
  };
  const handleChangePage = (page: number) => {
    setOffset(num * page);
  };

  return (
    <div className="container flex flex-column gap-2">
      <div
        style={{
          display: "flex",
          columnGap: "12px",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <input placeholder="Tên pokemon" value={keyword} onChange={handleChangeKeyword} />
        <div>Tổng cộng: {dataRes?.count ?? 0}</div>
      </div>
      <div className="flex flex-wrap gap-1">
        {Array.from(Array(count).keys()).map((val) => (
          <button key={val} onClick={() => handleChangePage(val)}>
            {val + 1}
          </button>
        ))}
      </div>
      {loading && (
        <progress className="pure-material-progress-linear"/>
      )}
      {!loading && (
        <div>
          <table className="table">
            <thead>
              <tr>
                <th style={{ width: "100px" }}>STT</th>
                <th style={{ width: "200px" }}>Tên pokemon</th>
              </tr>
            </thead>
            <tbody>
              {data?.map((val, i) => (
                <tr key={i + val?.name}>
                  <td align="center">{i + 1+offset}</td>
                  <td>{val?.name}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
}

export default TestReactJS;
