import './App.css';
import TestReactJS from './components/test/TestReactJS';

function App() {
  return (
   <TestReactJS />
  );
}

export default App;
